﻿# StemCSharp
1 C# class library for [STEM](https://gitlab.com/UnicornFreedom/stem "STEM") messaging protocol
## Function list:
This "library" is maximum transparent and does not provide socket connections it's just makes message decoding easier. You need implement socket connection independently. It's can be find in Program.cs file
* ``Stem.ChannelSubscribe(string id) // Subscribe (listen) the channel``
* ``Stem.ChannelUnSubscribe(string id) // Unsubscribe from channel``
* ``Stem.SendMessage(string id, string message) // Sends message to channel (subscription not required to send message)``
* ``Stem.DecodeMessage(byte[] message) // Decode message from STEM, returns a Stem.Message object``
## How to run?
Just open .sln file in Visual Studio or MonoDevelop on Linux and RUN!
## Project structure
``Program.cs`` - Simple Echo-test program (example)

``Stem.cs`` - class with all STEM-protocol functions (you can it use sepately)