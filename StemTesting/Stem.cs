﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace StemTesting
{
    class Stem
    {
        bool enableDBG = true; // Enable this for hex output packages

        public class Message
        {

            public string Channel;
            public string Text;
            public bool IsPing;
            public byte[] BinData;

            public Message(string channel, string message, bool isping, byte[] binary)
            {
                Channel = channel;
                Text = message;
                IsPing = isping;
                BinData = binary;
            }
        }


        private void PrintBytes(byte[] ba, [CallerMemberName] string callerName = "")
        {
            if (enableDBG)
            {
                StringBuilder hex = new StringBuilder(ba.Length * 2);
                foreach (byte b in ba)
                    hex.AppendFormat(" {0:x2} ", b);
                Console.WriteLine(callerName + ": " + hex.ToString());
            }
        }

        public byte[] ChannelSubscribe(string id)
        {
            var array = new byte[4];
            array[0] = 0x00; // some magic numbers
            array[2] = 0x01; // packet ID - subsribe to channel
            array[3] = System.Convert.ToByte(id.Length);
            array = array.Concat(Encoding.UTF8.GetBytes(id)).ToArray();
            array[1] = System.Convert.ToByte(array.Length - 2);

            PrintBytes(array);

            return array;
        }

        public byte[] ChannelUnSubscribe(string id)
        {
            var array = new byte[4];
            array[0] = 0x00; // some magic numbers
            array[2] = 0x02; // packet ID - unsubsribe from channel
            array[3] = System.Convert.ToByte(id.Length);
            array = array.Concat(Encoding.UTF8.GetBytes(id)).ToArray();
            array[1] = System.Convert.ToByte(array.Length - 2);

            PrintBytes(array);

            return array;
        }

        public byte[] SendMessage(string id, string message)
        {
            var array = new byte[4];
            array[0] = 0x00; // some magic numbers
            array[2] = 0x00; // send message packet
            array[3] = System.Convert.ToByte(id.Length);
            array = array.Concat(Encoding.UTF8.GetBytes(id)).Concat(Encoding.UTF8.GetBytes(message)).ToArray();
            array[1] = System.Convert.ToByte(array.Length - 2);

            PrintBytes(array);
            return array;
        }

        // This function is too buggy!

        public byte[] Pong(byte[] message)
        {
            var array = new byte[3];
            array[0] = 0x00; // some magic numbers
            array[2] = 0x04; // pong packet
            array = array.Concat(message).ToArray();
            array[1] = System.Convert.ToByte(array.Length - 2);

            PrintBytes(array);

            return array;
        }

        public Message DecodeMessage(byte[] message)
        {

            switch (message[2])
            {
                case 0x00:
                    int messageOffset = 4 + message[3];
                    var messageBytes = message.Skip(messageOffset).ToArray();
                    return new Message(System.Text.Encoding.UTF8.GetString(message, 4, message[3]), System.Text.Encoding.UTF8.GetString(messageBytes, 0, messageBytes.Length), false, messageBytes);
                case 0x03:
                    messageOffset = 3 + message[3];
                    messageBytes = message.Skip(messageOffset).ToArray();
                    return new Message(null, null, true, messageBytes);
                default:
                    throw new Exception("Invalid message");
            }

            //PrintBytes(message);
        }
    }
}
