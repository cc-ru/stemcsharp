﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace StemTesting
{
    class Program
    {
        public static int Main(String[] args)
        {
            BeginServer();
            return 0;
        }

        public static void PrintBytes(byte[] ba)
        {
                StringBuilder hex = new StringBuilder(ba.Length * 2);
                foreach (byte b in ba)
                    hex.AppendFormat(" {0:x2} ", b);
                Console.WriteLine(hex.ToString());
        }

        public static void BeginServer()
        {
            byte[] bytes = new byte[1024];


            try
            {
                Stem stem = new Stem(); // Init STEM class
                IPHostEntry host = Dns.GetHostEntry("stem.fomalhaut.me");
                IPAddress ipAddress = host.AddressList[0];
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, 5733);
   
                Socket sender = new Socket(ipAddress.AddressFamily,
                    SocketType.Stream, ProtocolType.Tcp);

                try
                {
                    // Connect to Remote EndPoint  
                    sender.Connect(remoteEP);

                    Console.WriteLine("STEM client connected to {0}",
                        sender.RemoteEndPoint.ToString());

                    // Encode the data string into a byte array.  

                    // Send the data through the socket.    
                    //var sevenItems = new byte[] { 0x00, 0x06, 0x01, 0x04, 0x74, 0x65, 0x73, 0x74 };
                    int bytesSent = sender.Send(stem.ChannelSubscribe("test"));
                    // Receive the response from the remote device.
                    while (true)
                    {
                        int bytesRec = sender.Receive(bytes);
                        Console.WriteLine("полученны данные.... = {0}", Encoding.UTF8.GetString(bytes, 0, bytesRec));
                        try
                        {
                            Stem.Message message = stem.DecodeMessage(bytes);
                            Console.WriteLine(message.Channel + " | " + message.Text);
                            if (!message.IsPing)
                            {
                                bytesSent = sender.Send(stem.SendMessage("test", "том йорк крутой чел!"));
                            }
                            else
                            {
                                bytesSent = sender.Send(stem.SendMessage("test", "получил пинг!"));
                                //PrintBytes(bytes);
                                bytesSent = sender.Send(stem.Pong(message.BinData)); // This function is too buggy!
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Unexpected while parse exception : {0}", e.ToString());
                        }
                        Array.Clear(bytes, 0, bytes.Length);
                    }
                    // Release the socket.    
                    //sender.Shutdown(SocketShutdown.Both);
                    //sender.Close();

                }
                catch (ArgumentNullException ane)
                {
                    Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
                }
                catch (SocketException se)
                {
                    Console.WriteLine("SocketException : {0}", se.ToString());
                }
                catch (Exception e)
                {
                    Console.WriteLine("Unexpected exception : {0}", e.ToString());
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
